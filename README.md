# CodeIgniter With JWT Authentication

# Notes

- Import Database from /db/rest_jwt.db
- Test it with postman/insomnia
- Create post method from postman for user authentication "http://localhost/contruction/api/auth/login"
- Add this to body multipart form :
	
	username = dodi
	
	password = dodi123

	- If your authentication success you will get generated token response
